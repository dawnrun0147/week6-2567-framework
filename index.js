const http = require('http')
const app = http.createServer((req,res) =>{
    if (req.url === "/") {
    res.writeHead(200, { "Content-Type": "text/html"});
    res.write("<h3>Home page<h3>");
    res.end();
    } else if(req.url === "/about") {
        res.writeHead(200, { "Content-Type": "text/plain"});
        res.write("<h3>About<h3>");
        res.end();
    } else if(req.url === "/admin") {
        res.writeHead(200, { "Content-Type": "text/html"});
        res.write("<h3>Admin<h3>");
        res.end();
    } else {
        res.writeHead(200, { "Content-Type": "text/html"});
        res.write("<h1 style='color:red'><center>404</center><h1>");
        res.end();
    }
});

 const PORT = 3000;
 const hostname = 'localhost';
app.listen(PORT,  hostname, ()=> {
    console.log("Server runnig at http://localhost:3000");
})